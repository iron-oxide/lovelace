# Lovelace

Notes on building a Rust math library for fun and use with [Project Euler](https://projecteuler.net/), [Cryptopals](https://cryptopals.com/) and the like.

## Build

    cargo install mdbook
    mdbook build
    mdbook serve
