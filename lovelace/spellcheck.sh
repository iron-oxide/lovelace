#!/bin/bash

# Build dictionary
echo "personal_ws-1.1 en 0 utf-8" > ./dictionary.txt

for f in dict/*.txt; do
    cat $f | sort -u >> ./tmp.txt;
done

sort -u tmp.txt >> ./dictionary.txt
rm ./tmp.txt

# Spellcheck
for f in src/*.md; do
    cat "$f" | aspell list --lang en_US --extra-dicts ./dictionary.txt --mode tex | sort -u;
    errors=$(cat "$f" | aspell list --lang en_US --extra-dicts ./dictionary.txt --mode tex | wc -l);
    if ! [ "$errors" -eq 0 ]; then
        exit 1;
    fi
done
