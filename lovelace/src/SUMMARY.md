# Lovelace

## Introduction

[Lovelace](./lovelace.md)
[Setup](./setup.md)

## Number Theory

- [Fibonacci](./ch01-00-fibonacci.md)
    - [Recursive](./ch01-01-recurseive-fibonacci.md)
    - [Closed Form](./ch01-02-closed-form-fibonacci.md)
    - [Polyphase merge sort](./ch01-03-polyphase-merge-sort.md)

- [Primes](./ch02-00-primes.md)
    - [Factoring by Division](./ch02-01-factoring-by-division.md)
    - [The rho Method](./ch02-02-rho-method.md)
    - [Fermat's Method](./ch02-03-Fermats-method.md)
    - [Factoring by Addition and Subtraction](./ch02-04-addition-subtraction.md)
    - [Factoring with Sieves](./ch02-05-sieves.md)
    - [Primality Testing](./ch02-06-primality-testing.md)
    - [Factoring by Continued Fractions](./ch02-07-continued-fractions.md)

- [Random](./ch03-00-random.md)

- [Bernoulli](./ch04-00-bernoulli.md)
