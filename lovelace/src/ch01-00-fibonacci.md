# Fibonacci

The Fibonacci numbers appear in nature and have fascinated scientists and artists. The number of petals on many flowers, the number of spirals on a sunflower, branching plants, and even the ratio between the forearm and the hand are occurrences of the Fibonacci numbers. They are also easy to calculate, starting with \\((1, 1)\\) add the last two numbers to derive the next.

\\[ \begin{alignat}{2}     &      &0  \\\\
                           &      &1  \\\\
                  0 + 1    &= \\: &1  \\\\
                  1 + 1    &= \\: &2  \\\\
                  1 + 2    &= \\: &3  \\\\
                  2 + 3    &= \\: &5  \\\\ 
                  3 + 5    &= \\: &8  \\\\ 
                  5 + 8    &= \\: &13 \\\\ 
                  8 + 13   &= \\: &21 \\\\ 
                  13 + 21  &= \\: &34 \\\\ 
                  21 + 34  &= \\: &55 \\\\ \end{alignat} \\]

Well that is easy enough to implement:

```rust
use std::io;

fn main() {
    let mut fib = 1;
    let mut last_fib = -1;
    let mut next_fib = 0;
    for i in 0..10 {
        next_fib = last_fib + fib;
        println!("The {} Fibonacci is: {}", i, next_fib);
        last_fib = fib;
        fib = next_fib;
    }
}
```

&nbsp;

But wait what is going with that initialization?

```rust,ignore
    let mut fib = 1;
    let mut last_fib = -1;
```

The sequence can be extended to negative numbers also using an alternating sequence. The recursive definition is:

\\[ F_n = F_{n-1} + F_{n-2} \\]

Which can be rewritten as:

\\[ F_{n-2} = F_n - F_{n-1} \\]

And it provides the alternating sequence:

\\[ F_{-n} = \left(-1\right)^{n+1}F_n \\]

Resulting in:

\\[ \begin{alignat}{2}   -3 - 5    &= \\: &-8 \\\\
                          2 - (-3) &= \\: &5  \\\\
                         -1 - 2    &= \\: &-3 \\\\
                          1 - (-1) &= \\: &2  \\\\
                          0 - 1    &= \\: &-1 \\\\
                          1 - 0    &= \\: &1  \\\\
                                   &      &0  \\\\
                                   &      &1  \\\\
                          0 + 1    &= \\: &1  \\\\
                          1 + 1    &= \\: &2  \\\\
                          1 + 2    &= \\: &3  \\\\
                          2 + 3    &= \\: &5  \\\\ 
                          3 + 5    &= \\: &8  \\\\ 
                          5 + 8    &= \\: &13 \\\\ 
                          8 + 13   &= \\: &21 \\\\ 
                          13 + 21  &= \\: &34 \\\\ 
                          21 + 34  &= \\: &55 \\\\ \end{alignat} \\]


&nbsp;

---

When we square them something interesting happens.

\\[ \begin{alignat}{9}  1 \\:&\\:  1 \\:&\\:  2  \\:&\\:  3 \\:&\\:  5 \\:&\\:  8 \\:&\\:   13 \\:&\\:  21 \\:&\\:   34 \\:&\\:   55  \\\\
                        1 \\:&\\:  1 \\:&\\:  4  \\:&\\:  9 \\:&\\: 25 \\:&\\: 64 \\:&\\:  169 \\:&\\: 441 \\:&\\: 1156 \\:&\\: 3025 \end{alignat} \\]

\\[ \begin{alignat}{2}  1 + 4    &= \\: &5  \\\\
                        4 + 9    &= \\: &13 \\\\
                        9 + 25   &= \\: &34 \\\\ \end{alignat} \\]

Adding the squares of the Fibonacci numbers gives \\(F_{n+2}\\).
