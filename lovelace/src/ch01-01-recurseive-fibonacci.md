# Recursive

The recursive sequence is defined as:

\\[ F_n = F_{n-1} + F_{n-2} \\]

Can we recurse it?

```rust
#use std::io;
#
#fn main() {
#    for i in 0..10 {
#        println!("The {} Fibonacci is: {}", i, nth_fib(i));
#    }
#}
#
fn nth_fib(n: i32) -> i32 {
    if n == 0 || n == 1 {
        return n
    }
    nth_fib(n-1) + nth_fib(n-2)
}
```

What about negative numbers? It should be possible to just calculate `nth_fin(n)` and then flip the sign accordingly.

```rust
#use std::io;
#
#fn main() {
#    for i in -9..10 {
#        println!("The {} Fibonacci is: {}", i, nth_fib(i));
#    }
#}
#
#fn nth_fib(n: i32) -> i32 {
#    if n == 0 || n == 1 {
#        return n
#    }
    if n < 0 {
        return (nth_fib(n.abs()-1)  + 
                nth_fib(n.abs()-2)) *
                -1_i32.pow((n.abs() + 1) as u32)
    }
#    nth_fib(n-1) + nth_fib(n-2)
#}
```

Missed the special case of -1 that causes a stack overflow.

```rust
#use std::io;
#
#fn main() {
#    for i in -9..10 {
#        println!("The {} Fibonacci is: {}", i, nth_fib(i));
#    }
#}
#
#fn nth_fib(n: i32) -> i32 {
#    if n == 0 || n == 1 {
#        return n
#    }
    if n == -1 {
        return 1
    }
#    if n < 0 {
#        return (nth_fib(n.abs()-1) + nth_fib(n.abs()-2)) * -1_i32.pow((n.abs() + 1) as u32)
#    }
#    nth_fib(n-1) + nth_fib(n-2)
#}
```

For correct order of operations `-1_i32` must be wrapped in parenthesis.

```rust
use std::io;

fn main() {
    for i in -9..10 {
        println!("The {} Fibonacci is: {}", i, nth_fib(i));
    }
}

fn nth_fib(n: i32) -> i32 {
    if n == 0 || n == 1 {
        return n
    }
    if n == -1 {
        return 1
    }
    if n < 0 {
        return (nth_fib(n.abs()-1) + nth_fib(n.abs()-2)) * (-1_i32).pow((n.abs() + 1) as u32)
    }
    nth_fib(n-1) + nth_fib(n-2)
}
```
